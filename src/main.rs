use std::fs::File;
use std::io::prelude::*;
use std::net::TcpStream;
use std::path::Path;
use ssh2::Session;
use std::thread;
extern crate rand;
use rand::Rng;
extern crate clap;
use clap::{Arg, App, SubCommand};

static username: &str = "username";
static password: &str = "password";

fn run_command(row: u8, column: u8, command: String) {
    let tcp = TcpStream::connect(format!("[fdcd::c:{}:{}]:22", row, column));
    if let Ok(t) = tcp {
        let tcp = t;
        let mut sess = Session::new().unwrap();
        sess.handshake(&tcp).unwrap();
        sess.userauth_password(username, password).unwrap();
        let mut channel = sess.channel_session().unwrap();
        channel.exec(&command).unwrap();
        let mut s = String::new();
        channel.read_to_string(&mut s).unwrap();
        println!("{}", s);
        channel.wait_close().unwrap();
    } else {
        println!("could not connect to ({}, {})", row, column);
    }
}

fn send_file(row: u8, column: u8, buffer: &Vec<u8>, filename: &str) {
    let tcp = TcpStream::connect(format!("[fdcd::c:{}:{}]:22", row, column));
    if let Ok(t) = tcp {
        let tcp = t;
        let mut sess = Session::new().unwrap();
        sess.handshake(&tcp).unwrap();
        sess.userauth_password(username, password).unwrap();

        //println!("buffer.len() = {}", buffer.len());
        let mut remote_file = sess.scp_send(Path::new(filename),
                                        0o644, buffer.len() as u64, None).expect("can't open remote file");
        remote_file.write_all(buffer).expect("can't write file");
        //remote_file.flush().expect("can't flush on remote file");
    } else {
        println!("could not connect to ({}, {})", row, column);
    }
}


fn main() {
    //let command = "DISPLAY=\":0\" firefox \"https://www.youtube.com/watch?v=ZZ5LpwO-An4\"";
    //let command = "DISPLAY=\":0\" firefox \"https://upload.wikimedia.org/wikipedia/commons/9/9f/Gennady_Korotkevich.jpg\"";
    //let command = "DISPLAY=\":0\" firefox \"https://www.youtube.com/watch?v=G1IbRujko-A\"";
    //let command = "DISPLAY=\":0\" firefox \"https://www.youtube.com/watch?v=6Dh-RL__uN4\"";
    //let command = "killall firefox";
    //let command = "echo test";
    //let command = "gnome-terminal --full-screen -- cmatrix -b -C cyan &";
    //let command = "git clone \"https://github.com/klange/nyancat\"; cd nyancat && make && cd src && gnome-terminal --full-screen -- ./nyancat";


    let matches = App::new("remote")
        .version("0.1")
        .arg(Arg::with_name("target")
             .short("t")
             .long("target")
             .value_name("TARGET")
             .help("specifies target, defaults to everyone\naccepted values are all, rand and a1 ... c4")
             .takes_value(true))
        .subcommand(SubCommand::with_name("send")
                    .about("loads file remotely")
                    .arg(Arg::with_name("file")
                         .required(true)
                         .index(1)
                         .help("file to send")))
        .subcommand(SubCommand::with_name("run")
                    .about("runs command remotely")
                    .arg(Arg::with_name("command")
                         .required(true)
                         .index(1)
                         .help("command to run")))
        .subcommand(SubCommand::with_name("firefox")
                    .about("open web page remotely")
                    .arg(Arg::with_name("url")
                         .required(true)
                         .index(1)
                         .help("url to visit")))
        .get_matches();

    let mut targets = vec![];

    let tstr = matches.value_of("target").unwrap_or("all").to_lowercase();

    if tstr == "all" {
        for r in 1..=4 {
            for c in 1..=4 {
                targets.push((r, c));
            }
        }
    } else if tstr == "rand" {
        let mut rng = rand::thread_rng();
        targets.push((rng.gen_range(1, 5), rng.gen_range(1, 5)));
    } else {
        let chars: Vec<char> = tstr.chars().collect();
        if tstr.len() == 2 && 'a' <= chars[0] && chars[0] <= 'd' && '1' <= chars[1] && chars[1] <= '4' {
            targets.push((chars[0] as u8 - 'a' as u8 + 1, chars[1] as u8 - '0' as u8));
        } else {
            println!("malformed target string!\nexamples of valid targets are A2, B4, all, c3, rand");
            return;
        }
    }

    let mut command = String::new();
    if let Some(matches) = matches.subcommand_matches("run") {
        //println!("running command {}", matches.value_of("command").unwrap());
        command = matches.value_of("command").unwrap().to_owned();
    } else if let Some(matches) = matches.subcommand_matches("firefox") {
        command = format!("DISPLAY=\":0\" firefox \"{}\"", matches.value_of("url").unwrap()).to_owned();
    }

    if command != "" {
        let mut handles = vec![];
        for (row, column) in targets {
            let c = command.clone();
            let t = thread::spawn(move || {
                println!("({}, {})", row, column);
                run_command(row, column, c);
            });
            handles.push(t);
        }

        for h in handles {
            h.join().unwrap();
        }
    } else if let Some(matches) = matches.subcommand_matches("send") {
        //println!("sending file {}", matches.value_of("file").unwrap());
        let filename = matches.value_of("file").unwrap().to_owned();
        let mut f = File::open(&filename).expect("can't open file!");

        let mut buffer = Vec::new();
        f.read_to_end(&mut buffer).expect("can't read file!");
        
        let mut handles = vec![];
        for (row, column) in targets {
            let b = buffer.clone();
            let f = filename.clone();
            let t = thread::spawn(move || {
                println!("({}, {})", row, column);
                send_file(row, column, &b, &f);
            });
            handles.push(t);
        }

        for h in handles {
            h.join().unwrap();
        }
    } else {
        println!("run with --help to show available options")
    }
}
